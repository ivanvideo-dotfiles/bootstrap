#!/bin/bash
#  _______         ____         ____    _______________     _____    _______________  _______        ____      _______  
# /______/ \     /___ / \     /___ / \ /______________/   /____ / \ /______________/ /______/ \    /____/ \   /______/ \
#| |     \ /|  /|     \ /|  /|     \ /|       | |       /|      \ /        | |      | |     \ /| /|     \ /| | |     \ /|
#| |______|/  | |      | | | |      | |       | |      |/ \ _____          | |      | |______|/ | |______| | | |______|/
#|/______/ \  | |      | | | |      | |       | |       \ /____ / \        | |      |/______/   |/_______| | |/______/  
#| |     \ /| | |      | | | |      | |       | |               \ /|       | |      | | \   \   | |      | | | |        
#| |______|/  |/ \ ____|/  |/ \ ____|/        | |       / \ _____|/        | |      | |   \   \ | |      | | | |        
#|/______/     \ /____/     \ /____/          |/        \ /_____/          |/       |/      \ / |/       |/  |/         
#
#by: victor@isan.ro

# create dirs

createDirs() {
mkdir -p ~/Code/suckless
mkdir ~/.config
mkdir ~/.cache/zsh
curl https://gitlab.com/-/snippets/2448751/raw/main/user-dirs.dirs > ~/.config/user-dirs.dirs
}

installPackages() {
# check distro
. /etc/os-release

# install packages
echo "installng packages..."

case $ID in
	void)
	# update
	sudo xbps-install -Su
	# packages
	sudo xbps-install vim alacritty rofi git zsh librewolf curl wget ranger unzip
	# xorg
	sudo xbps-install xorg libX11-devel libXft-devel libXinerama-devel base-devel
	;;

	arch|artix)
	# update
	sudo pacman -Syu
	# neofetch
	sudo pacman -S neofetch
	# packages
	sudo pacman -S vim alacritty rofi git zsh librewolf curl wget unzip 
	ranger
	# xorg
	sudo pacman -S xorg xorg-xinit xorg-server libX11 libXft libXinerama base-devel
	# yay
	git clone https://aur.archlinux.org/yay.git /tmp/yay
	cd /tmp/yay
	makepkg -si
	# additional
	yay -S xarchiver pcmanfm polkit mate-polkit
	;;

	debian|ubuntu|linuxmint|kali)
	# update
	sudo apt update
	sudo apt upgrade
	# packages
	sudo apt install vim rofi git zsh librewolf-esr curl wget ranger unzip
	##alacritty
	wget -O /tmp/alacritty.deb https://github.com/barnumbirr/alacritty-debian/releases/download/v0.11.0-1/alacritty_0.11.0-1_amd64_bullseye.deb
	sudo apt install /tmp/alacritty.deb
	# xorg
	sudo apt install xorg libx11-dev libxft-dev libxinerama-dev libx11-xcb-dev libxcb-res0-dev build-essential
	;;
	*)
	echo "unknown os-release, aborting..."
	exit 0
	;;
esac

# install dwm
echo "installing dwm"

git clone https://gitlab.com/ivanvideo-dotfiles/dwm ~/Code/suckless/dwm
cd ~/Code/suckless/dwm
sudo make clean install
echo "exec dwm" > ~/.xinitrc
}

configZsh() {
# install zsh
sudo git clone https://github.com/zsh-users/zsh-autosuggestions /usr/share/zsh/plugins/zsh-autosuggestions
sudo git clone https://github.com/zsh-users/zsh-syntax-highlighting /usr/share/zsh/plugins/zsh-syntax-highlighting

curl https://gitlab.com/-/snippets/2448744/raw/main/zshrc > ~/.config/zshrc
ln -s ~/.config/zshrc ~/.zshrc
sudo usermod -s /bin/zsh $USER
}

configRofi() {
# rofi config
mkdir ~/.config/rofi
curl https://gitlab.com/-/snippets/2448745/raw/main/config.rasi > ~/.config/rofi/config.rasi
}

configScreenlayout() {
# screenlayout
mkdir ~/.screenlayout/
curl https://gitlab.com/-/snippets/2448747/raw/main/home.sh > ~/.screenlayout/home.sh
}

configGtk() {
# gtk
wget -O /tmp/draculagtk.zip https://github.com/dracula/gtk/archive/master.zip  
unzip /tmp/draculagtk.zip -d /tmp
sudo mv /tmp/gtk-master /usr/share/themes/dracula

wget -O /tmp/draculaicons.zip https://github.com/dracula/gtk/files/5214870/Dracula.zip  
unzip /tmp/draculaicons.zip -d /tmp
sudo mv /tmp/Dracula /usr/share/icons/Dracula

mkdir ~/.config/gtk-3.0
curl https://gitlab.com/-/snippets/2448748/raw/main/settings.ini > ~/.config/gtk-3.0/settings.ini
}

echo "Select option number:"
echo "1. full"
echo "2. zsh (must be installed)"
read -r option

case $option in 
	1)
		createDirs;
		installPackages;
		configZsh;
		configRofi;
		configScreenlayout;
		configGtk;
	;;

	2)
		mkdir .cache/zsh;
		configZsh;
	;;

	*)
		echo Option not specified. Aborting...
	;;
esac

#TODO: hotkeys
